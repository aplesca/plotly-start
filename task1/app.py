# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

df = pd.read_csv(
    './SummaryAcquisitions_auto_generated.csv', nrows=100)
# read last rows
df_new = df[['voltage', 'efficiency', 'rpc', 'path']].copy()

grouped = df_new.groupby(["rpc", "path"])

# todo
# make it more clear
plots = [dcc.Graph(id=str(gname), figure=px.scatter(group, x='voltage', y='efficiency'))
         for gname, group in grouped]

app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children=plots),


    dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in df_new.columns],
        data=df_new.to_dict('records'),
    )
])


if __name__ == '__main__':
    app.run_server(debug=True)
