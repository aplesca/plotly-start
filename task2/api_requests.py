import requests


def post_parameter(slot, channel, parameter, input_val):
    # 2 1 I0Set 2
    base_uri = 'https://epdt-hv-controller.app.cern.ch/set'
    params = {
        'module': 'hvrpc256.cern.ch',
        'username': 'rpcgif',
        'password': 'RPCgas2021',
        'slot': slot,
        'channels': channel,
        'parameter': parameter,
        'value': input_val
    }

    r = requests.get(base_uri, params=params)
    return r


def det_display_message(r):
    if r.status_code == 200:
        return f'{r.status_code}: {r.json()["message"]}'
    else:
        return f'{r.status_code}: {r.json()["detail"][0]["msg"]}'
