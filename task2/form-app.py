import dash
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import dash_table
import dash_core_components as dcc
import dash_html_components as html

from websocket import get_data, start_ws_in_thread
from api_requests import post_parameter, det_display_message

thread = start_ws_in_thread()

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

form_alerts = html.Div(id='form-alerts-div', children=[])
table_alerts = html.Div(id='table-alerts-div', children=[])

specific_input = dcc.Input(
    id="input_number",
)

form_layout = html.Form([

    dcc.Dropdown(
        id='slot-dropdown',
        options=[
            {'label': 'Slot 0', 'value': '0'},
            {'label': 'Slot 1', 'value': '1'},
            {'label': 'Slot 2', 'value': '2'},
        ],
        value='2'
    ),

    dcc.Dropdown(
        id='channel-dropdown',
        options=[
            {'label': 'Channel 0', 'value': '0'},
            {'label': 'Channel 1', 'value': '1'},
            {'label': 'Channel 2', 'value': '2'},
            {'label': 'Channel 3', 'value': '3'},
            {'label': 'Channel 4', 'value': '4'},
            {'label': 'Channel 5', 'value': '5'},
        ],
        value='0'
    ),

    dcc.Dropdown(
        id='parameter-dropdown',
        options=[
            {'label': 'I0Set', 'value': 'I0Set'},
            {'label': 'V0Set', 'value': 'V0Set'},
            {'label': 'Pw', 'value': 'Pw'},
            {'label': 'Correction', 'value': 'Correction'}
        ],
        value='I0Set',
    ),

    html.Div(id='specific_input', children=[specific_input]),

    html.Button('Submit', id='submit-val', type='button',
                n_clicks=0),  # , type='button' without it, page refreshes
])

app.layout = html.Div([
    form_alerts,
    table_alerts,

    dash_table.DataTable(
        id='table-editing-simple',
    ),

    dcc.Interval(
        id='live-table-update',
        interval=1*1000,  # in milliseconds
        n_intervals=0
    ),

    form_layout,
])


@app.callback(
    Output('table-editing-simple', 'columns'),
    Output('table-editing-simple', 'data'),
    Output('table-alerts-div', 'children'),
    Input('live-table-update', 'n_intervals'))
def update_table(n):
    df = get_data()

    alert = None
    if df.empty:
        alert = dbc.Alert('No table data from websocket',
                          color="warning", dismissable=True)

    columns = [{
        "name": str(i),
        "id": i,
    } for i in df.columns],

    return columns[0], df.to_dict('records'), alert


@app.callback(
    Output('form-alerts-div', 'children'),
    Input('submit-val', 'n_clicks'),

    State('slot-dropdown', 'value'),
    State('channel-dropdown', 'value'),
    State('parameter-dropdown', 'value'),
    State('input_number', 'value'),)
def update_output(n_clicks, slot, channel, parameter, input):

    if n_clicks > 0:
        # print(slot, channel, parameter, input)
        r = post_parameter(slot, channel, parameter, input)
        # print(r.json())
        return dbc.Alert(det_display_message(r), color="primary", dismissable=True)


@app.callback(
    Output('specific_input', 'children'),
    Input('parameter-dropdown', 'value'))
def parameter_type_input(parameter):
    if parameter == "Pw" or parameter == "Correction":
        return dcc.Dropdown(
            id='input_number',
            options=[
                {'label': 'On', 'value': '1'},
                {'label': 'Off', 'value': '0'},
            ],
            value='0'
        )
    elif parameter == "I0Set":
        return dcc.Input(
            id="input_number",
            type="number",
            min=0, max=1000,
            placeholder="input type number",
        )
    elif parameter == "V0Set":
        return dcc.Input(
            id="input_number",
            type="number",
            min=0, max=12000,
            placeholder="input type number",
        )


if __name__ == '__main__':
    app.run_server(debug=True)
