
import json
import pandas as pd  # pip install pandas
import websockets  # pip install websockets
import asyncio
import threading as th


def get_data():
    return DATAFRAME


def shape_data(original_df):
    # get values
    value_df = original_df.pivot(
        index=["slot", "channel"],
        columns="parameter",
        values=["value"]
    ).stack([0]).reset_index()

    # get correction
    correction_df = original_df.pivot(
        index=["slot", "channel"],
        columns="parameter",
        values=["correction"]
    ).stack([0]).reset_index()

    # get needed columns
    new_df = value_df[["slot", "channel", "V0Set", "I0Set", "VMon", "IMon",
                       "Pw", "Status"]].copy()

    new_df["Correction(V0Set)"] = correction_df["V0Set"]

    return new_df


async def hv_controller(module: str):
    global DATAFRAME
    DATAFRAME = pd.DataFrame()
    uri = f"wss://epdt-hv-controller.app.cern.ch/subscribe?module={module}&slot=2&channels=0,1&parameter=V0Set"

    async with websockets.connect(uri) as websocket:
        while True:
            msg = await websocket.recv()
            try:
                # os.system("clear")
                msg = json.loads(msg)
                df = pd.DataFrame.from_records(msg)
                # print(df)

                DATAFRAME = shape_data(df)
                # print(DATAFRAME)
            except Exception as e:
                print(e)

module = "hvrpc256.cern.ch"


def start_websocket(rpc=module):
    asyncio.new_event_loop().run_until_complete(hv_controller(rpc))


def start_ws_in_thread():
    # start websocket in another thread
    t = th.Thread(target=start_websocket)
    t.start()
    return t
