import dash
from dash.dependencies import Input, Output, State
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd

from demo import get_data, start_ws_in_thread

thread = start_ws_in_thread()


app = dash.Dash(__name__)

editable_columns = ['V0Set', 'I0Set', 'Pw']

app.layout = html.Div([
    dash_table.DataTable(
        id='table-editing-simple',
        editable=True
    ),
    html.Div(id='table-editing-simple-output'),

    dcc.Interval(
        id='live-table-update',
        interval=1*1000,  # in milliseconds
        n_intervals=0
    ),
    dcc.Interval(id="selection-timeout",
                 interval=10 * 1000,
                 n_intervals=0,
                 disabled=True),

    dcc.Store(id="store-n-intervals"),


])


@app.callback(
    Output('table-editing-simple', 'columns'),
    Output('table-editing-simple', 'data'),
    Input('live-table-update', 'n_intervals'))
def update_table(n):

    print("updating")

    df = get_data()

    columns = [{
        "name": str(i),
        "id": i,
        'editable': False if i not in editable_columns else True
    }
        for i in df.columns],

    return columns[0], df.to_dict('records')


# @app.callback(
#     Output('table-editing-simple-output', 'children'),
#     Input('table-editing-simple', 'data'),
#     Input('table-editing-simple', 'columns'))
# def display_output(rows, columns):
#     print('editin simple')
#     # start_edit()
#     # print(type(interval_component))
#     # print(interval_component.interval)
#     # setattr(interval_component, 'disabled', 'True')
#     # print(interval_component.interval)
#     df = pd.DataFrame(rows, columns=[c['name'] for c in columns])

#     return 'some ouput'


@app.callback(Output("live-table-update", "disabled"),
              Output("selection-timeout", "disabled"),
              Output("store-n-intervals", "data"),
              Input("selection-timeout", "n_intervals"),
              Input("table-editing-simple", "active_cell"),
              State("store-n-intervals", "data"))
def enable_again_live_updates(n_intervals, active_cell, store_n_intervals):
    if store_n_intervals is None:
        store_n_intervals = {"n_intervals": n_intervals}
    ctx = dash.callback_context
    if ctx.triggered[0]["prop_id"] == "table-editing-simple.active_cell":
        print(
            f"Active cell triggered: disabling live updates and enabling timeout"
        )
        return True, False, store_n_intervals
    else:
        print(
            f"Selection timeout triggered: active cell is {active_cell}. n_intervals={n_intervals}, store={store_n_intervals}"
        )
        if store_n_intervals["n_intervals"] < n_intervals:
            print("Timeout passed: re enabling live updated")
            return False, True, store_n_intervals

    return False, True, store_n_intervals


if __name__ == '__main__':
    app.run_server(debug=True)
