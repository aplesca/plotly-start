# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import dash_ace
import pprint
from schema import validate_config

import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])


with open("demo_config.yml", "r") as f:
    conf = f.read()

donf_as_dict = yaml.load(conf, Loader=Loader)
print(validate_config(donf_as_dict))

# ace editor needs a string as a value
conf_as_string = pprint.pformat(donf_as_dict, width=60, indent=2)

yaml_alerts = html.Div(id='yaml-alerts-div', children=[])


@app.callback(
    Output('yaml-alerts-div', 'children'),
    Input('submit-val', 'n_clicks'),
    State('ace-input', 'value'),)
def update_output(n_clicks, yaml_input):
    config_dict = yaml.load(yaml_input, Loader=Loader)
    if n_clicks > 0:
        r = validate_config(config_dict)
        return [dbc.Alert(r, color="primary", dismissable=True)]


app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),
    yaml_alerts,
    html.Button('Submit', id='submit-val', type='button',
                n_clicks=0),

    dash_ace.DashAceEditor(
        id='ace-input',
        value=conf,
        theme='monokai',
        mode='python',
        tabSize=2,
        enableBasicAutocompletion=True,
        enableLiveAutocompletion=True,
        placeholder='Python code ...'
    ),



])


if __name__ == '__main__':
    app.run_server(debug=True)
