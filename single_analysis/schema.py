from typing import List, Tuple, Dict, Optional
from pydantic import BaseModel, validator, Field, ValidationError
from pathlib import Path


class Wave(BaseModel):
    rpc: int
    strip: int


class Acquisition(BaseModel):
    header: int = 520
    record_length: int = 520
    strip_map: Dict[str, Wave]
    header_lines: Optional[int] = None
    trigger: str = "wave0"


class Digitizer(BaseModel):
    adc_to_mv: float = None
    bit_resolution: int
    sample_frequency: int
    time_resolution: float = None
    vpp: float

    # gives errors
    # @validator("adc_to_mv", always=True)
    # def calculation_adc_to_mv(cls, v, values, **kwargs):
    #     return values['vpp'] / 2**values['bit_resolution'] * 1e3

    @validator("time_resolution", always=True)
    def calculation_time_resolution(cls, v, values, **kwargs):
        return 1 / values['sample_frequency'] * 1e3


class Rpc(BaseModel):
    resistance: float = 56


class Run(BaseModel):
    path: Path = Field(None)
    correction_factor: int = 1
    currents_path: str = 'currents.xlsx'
    efficiency_parameters_limits: Tuple[
        Tuple[float, float, float],
        Tuple[float, float, float]
    ] = ((0, 1e-4, 6000), (1, 0.2, 12000))
    fit_params: List[int] = Field(
        [0.9, 0.01, 9800], alias='initial_params')
    gas_mixture: Dict[str, float]
    initial_params: List[int] = Field(
        [0.9, 0.01, 9800], alias='fit_params')
    mix_folder: str = "MIX0"
    p: float = 965
    p0: float = 965
    parameter_map: Dict[int, int] = None
    t: float = 20
    t0: float = 20
    working_point_delta: int = 150


class Signal(BaseModel):
    axis: int = 1
    baseline_interval: List[int] = [0, 200]
    baseline_threshold: int = 5
    charge_factor: float = 0.122 * 2 / 56
    charge_region: List[int] = [5, 30]
    charge_thresh: float = 9
    charge_thresh_av: float = 5
    charge_threshold: float = 0.15
    height_thresh: float = 2
    height_thresh_av: float = 12
    heights_difference_threshold: float = 1
    heights_ratio_threshold: float = 1
    noise_counts: float = 2
    noise_thresh: float = 9
    time_max: int = 400
    time_min: int = 280
    tot_region: List[int] = [10, 30]


class Config(BaseModel):
    acquisition: Acquisition
    digitizer: Digitizer
    rpc: Rpc
    run: Run
    signal: Signal


def validate_config(dict):
    response = 'Data is valid'
    try:
        Config(**dict)
    except ValidationError as e:
        response = e.json()
    return response
